package com.icefire.cipher.services;

import com.icefire.cipher.exceptions.UserAlreadyExistsException;
import com.icefire.cipher.models.User;
import com.icefire.cipher.models.UserKeyPair;
import com.icefire.cipher.repositories.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class UserServiceTest {

    @Mock
    private UserKeyPairService userKeyPairService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserService userService;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void createUser() throws Exception {
        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        String mockedUsername = "guilherme";

        when(userKeyPairService.generateAndCreateKeyPair()).thenReturn(mock(UserKeyPair.class));

        userService.createUser(mockedUsername, "123456");

        verify(userRepository).save(userCaptor.capture());
        User userCaptured = userCaptor.getValue();
        assertEquals(userCaptured.getUsername(), mockedUsername);
    }

    @Test(expected = UserAlreadyExistsException.class)
    public void createUserThatAlreadyExists() throws Exception {
        String mockedUsername = "guilherme";

        when(userRepository.findByUsername(mockedUsername)).thenReturn(mock(User.class));

        userService.createUser(mockedUsername, "123456");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createUserWithoutUsername() throws Exception {
        userService.createUser(null, "123456");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createUserWithoutPassword() throws Exception {
        userService.createUser("username", null);
    }

    @Test
    public void getUserByUsername() {
        User user = mockedUser("guilherme");

        when(userRepository.findByUsername(user.getUsername())).thenReturn(user);

        User foundUser = userService.getUserByUsername(user.getUsername());
        assertEquals(user.getUsername(), foundUser.getUsername());
    }

    @Test(expected = UsernameNotFoundException.class)
    public void getUserByUsernameNotFound() {
        User user = mockedUser("guilherme");

        when(userRepository.findByUsername(user.getUsername())).thenReturn(null);

        userService.getUserByUsername(user.getUsername());
    }

    private User mockedUser(String mockedUsername) {
        User user = mock(User.class);
        when(user.getUsername()).thenReturn(mockedUsername);
        when(user.getPassword()).thenReturn("123456");
        return user;
    }
}