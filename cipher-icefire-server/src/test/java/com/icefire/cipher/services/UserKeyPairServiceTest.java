package com.icefire.cipher.services;

import com.icefire.cipher.models.UserKeyPair;
import com.icefire.cipher.repositories.UserKeyPairRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.*;

public class UserKeyPairServiceTest {

    @Mock
    private UserKeyPairRepository userKeyPairRepository;

    @InjectMocks
    private UserKeyPairService userKeyPairService;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void generateAndCreateKeyPair() {
        userKeyPairService.generateAndCreateKeyPair();

        verify(userKeyPairRepository).save(any(UserKeyPair.class));
    }
}