package com.icefire.cipher.services;

import com.icefire.cipher.exceptions.UserCannotDecryptException;
import com.icefire.cipher.models.CipherValue;
import com.icefire.cipher.models.User;
import com.icefire.cipher.models.UserKeyPair;
import com.icefire.cipher.repositories.CipherValuesRepository;
import com.icefire.cipher.services.dto.EncryptedValue;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.*;
import static java.util.Collections.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class CipherServiceTest {

    @Mock
    private UserService userService;

    @Mock
    private CipherValuesRepository cipherValuesRepository;

    @InjectMocks
    private CipherService cipherService;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void encryptDataForUser() throws Exception {
        String mockedUsername = "username";
        User mockedUser = mockUser(mockedUsername);

        when(userService.getUserByUsername(mockedUsername)).thenReturn(mockedUser);

        cipherService.encryptDataForUser("username","test");

        verify(cipherValuesRepository).save(Mockito.any(CipherValue.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void encryptDataForUserInvalidValue() throws Exception {
        String mockedUsername = "username";
        User mockedUser = mockUser(mockedUsername);

        when(userService.getUserByUsername(mockedUsername)).thenReturn(mockedUser);

        cipherService.encryptDataForUser("username",null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void encryptDataForUserInvalidUser() {
        when(userService.getUserByUsername(null)).thenCallRealMethod();

        cipherService.encryptDataForUser(null,"teste");
    }

    @Test
    public void listEncryptDateForUser() throws Exception {
        User mockedUser = mockUser("guilherme");
        String mockedEncryptedValue = "encrypted";
        CipherValue cipherValue = new CipherValue(mockedUser, mockedEncryptedValue);

        when(userService.getUserByUsername(mockedUser.getUsername())).thenReturn(mockedUser);
        when(cipherValuesRepository.findByUserId(mockedUser.getId())).thenReturn(asList(cipherValue));

        List<EncryptedValue> encryptedValues = cipherService.listEncryptDateForUser(mockedUser.getUsername());

        assertEquals(encryptedValues.size(), 1);
        assertEquals(encryptedValues.get(0).getEncryptedValue(), mockedEncryptedValue);
    }

    @Test
    public void listEncryptDateForUserEmpty() throws Exception {
        User mockedUser = mockUser("guilherme");

        when(userService.getUserByUsername(mockedUser.getUsername())).thenReturn(mockedUser);
        when(cipherValuesRepository.findByUserId(mockedUser.getId())).thenReturn(emptyList());

        List<EncryptedValue> encryptedValues = cipherService.listEncryptDateForUser(mockedUser.getUsername());

        assertEquals(encryptedValues.size(), 0);
    }

    @Test
    public void decryptDataForUser() throws Exception {
        String expectedDecrypted = "teste";
        User mockedUser = mockUser("guilherme");

        when(userService.getUserByUsername(mockedUser.getUsername())).thenReturn(mockedUser);

        EncryptedValue encryptedValue = cipherService.encryptDataForUser(mockedUser.getUsername(),expectedDecrypted);

        String decryptedResult = cipherService.decryptDataForUser("guilherme",encryptedValue.getEncryptedValue());

        assertEquals(expectedDecrypted, decryptedResult);
    }

    @Test(expected = IllegalArgumentException.class)
    public void decryptDataForUserNullValue() throws Exception {
        User mockedUser = mockUser("guilherme");

        when(userService.getUserByUsername(mockedUser.getUsername())).thenReturn(mockedUser);

        EncryptedValue encryptedValue = cipherService.encryptDataForUser(mockedUser.getUsername(),null);

        cipherService.decryptDataForUser("guilherme",encryptedValue.getEncryptedValue());
    }

    @Test(expected = UserCannotDecryptException.class)
    public void decryptDataForUserCannotAbleToDecrypt() throws Exception {
        String expectedDecrypted = "teste";
        User mockedUser = mockUser("guilherme");
        User mockedUser2 = mockUser("guilherme2");

        when(userService.getUserByUsername(mockedUser.getUsername())).thenReturn(mockedUser);

        EncryptedValue encryptedValue = cipherService.encryptDataForUser(mockedUser.getUsername(),expectedDecrypted);

        when(userService.getUserByUsername(mockedUser2.getUsername())).thenReturn(mockedUser2);

        cipherService.decryptDataForUser("guilherme2",encryptedValue.getEncryptedValue());
    }

    private User mockUser(String username) throws Exception {
        User user = mock(User.class);
        UserKeyPair userKeyPair = generateKeyPair();
        when(user.getId()).thenReturn(1L);
        when(user.getUsername()).thenReturn(username);
        when(user.getUserKeyPair()).thenReturn(userKeyPair);
        return user;
    }

    private UserKeyPair generateKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        byte[] privateKey = keyPair.getPrivate().getEncoded();
        byte[] publicKey = keyPair.getPublic().getEncoded();

        return new UserKeyPair(privateKey, publicKey);
    }
}