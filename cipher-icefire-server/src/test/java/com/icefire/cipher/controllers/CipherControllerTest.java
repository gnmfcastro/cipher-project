package com.icefire.cipher.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.icefire.cipher.services.dto.EncryptedValue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static com.icefire.cipher.security.services.JWTAuthenticationService.createToken;
import static java.lang.String.format;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
@RunWith(SpringRunner.class)
public class CipherControllerTest {

    private static String AUTH_HEADER = "X-Authorization";

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc.perform(post("/users")
                .contentType(APPLICATION_JSON)
                .content("{\"username\":\"guilherme\", \"password\":\"1234\"}"))
                .andExpect(status().isOk());
    }

    @Test
    public void listMyEncryptedValuesWithoutAuth() throws Exception {
        mockMvc.perform(get("/ciphers"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void listMyEncryptedValuesEmptyValue() throws Exception {
        String token = createToken("guilherme");
        mockMvc.perform(get("/ciphers").header(AUTH_HEADER, token))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    @Test
    public void listMyEncryptedValuesWithValues() throws Exception {
        String token = createToken("guilherme");

        mockMvc.perform(post("/ciphers/encrypt")
                .header("X-Authorization", token)
                .contentType(APPLICATION_JSON)
                .content("{\"value\":\"teste\"}")
        ).andExpect(status().isOk());

        mockMvc.perform(get("/ciphers").header(AUTH_HEADER, token))
                .andExpect(status().isOk())
                .andExpect(content().json("[{}]"));
    }

    @Test
    public void encryptWithoutAuth() throws Exception {
        mockMvc.perform(post("/ciphers/encrypt")
                .contentType(APPLICATION_JSON)
                .content("{\"value\":\"teste\"}")
        ).andExpect(status().isForbidden());
    }

    @Test
    public void encrypt() throws Exception {
        String token = createToken("guilherme");

        mockMvc.perform(post("/ciphers/encrypt")
                .header(AUTH_HEADER, token)
                .contentType(APPLICATION_JSON)
                .content("{\"value\":\"teste\"}")
        ).andExpect(status().isOk())
                .andExpect(content().json("{}"));
    }

    @Test
    public void encryptDataWithoutValue() throws Exception {
        String token = createToken("guilherme");

        mockMvc.perform(post("/ciphers/encrypt")
                .header(AUTH_HEADER, token)
                .contentType(APPLICATION_JSON)
                .content("{\"value\":\"\"}")
        ).andExpect(status().isBadRequest());

        mockMvc.perform(post("/ciphers/encrypt")
                .header(AUTH_HEADER, token)
                .contentType(APPLICATION_JSON)
                .content("{}")
        ).andExpect(status().isBadRequest());
    }


    @Test
    public void decrypt() throws Exception {
        String token = createToken("guilherme");

        MvcResult result = mockMvc.perform(post("/ciphers/encrypt")
                .header(AUTH_HEADER, token)
                .contentType(APPLICATION_JSON)
                .content("{\"value\":\"teste\"}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{}"))
                .andReturn();

        EncryptedValue encryptedValue = new ObjectMapper().readValue(result.getResponse().getContentAsString(), EncryptedValue.class);

        mockMvc.perform(get(format("/ciphers/decrypt/%s",encryptedValue.getEncryptedValueUrlSafe()))
                .header(AUTH_HEADER, token))
                .andExpect(status().isOk())
                .andExpect(content().string("teste"));
    }

    @Test
    public void decryptWithoutAuth() throws Exception {
        mockMvc.perform(get("/ciphers/decrypt/asdasd")).andExpect(status().isForbidden());
    }

    @Test
    public void decryptInvalidValue() throws Exception {
        String token = createToken("guilherme");

        mockMvc.perform(post("/ciphers/encrypt")
                .header(AUTH_HEADER, token)
                .contentType(APPLICATION_JSON)
                .content("{\"value\":\"teste\"}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{}"))
                .andReturn();

        mockMvc.perform(get("/ciphers/decrypt/teste")
                .header(AUTH_HEADER, token))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void userTryToDecryptValueFromAnotherUser() throws Exception {
        String token = createToken("guilherme");

        MvcResult result = mockMvc.perform(post("/ciphers/encrypt")
                .header(AUTH_HEADER, token)
                .contentType(APPLICATION_JSON)
                .content("{\"value\":\"teste\"}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{}"))
                .andReturn();

        EncryptedValue encryptedValue = new ObjectMapper().readValue(result.getResponse().getContentAsString(), EncryptedValue.class);

        mockMvc.perform(post("/users")
                .contentType(APPLICATION_JSON)
                .content("{\"username\":\"guilherme2\", \"password\":\"1234\"}"))
                .andExpect(status().isOk());
        String token2 = createToken("guilherme2");

        mockMvc.perform(get(format("/ciphers/decrypt/%s",encryptedValue.getEncryptedValueUrlSafe()))
                .header(AUTH_HEADER, token2))
                .andExpect(status().isUnprocessableEntity());
    }
}