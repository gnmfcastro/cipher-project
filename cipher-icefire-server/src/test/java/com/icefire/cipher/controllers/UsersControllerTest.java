package com.icefire.cipher.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.annotation.DirtiesContext.MethodMode.AFTER_METHOD;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class UsersControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DirtiesContext( methodMode = AFTER_METHOD )
    public void createUser() throws Exception {
        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\":\"guilherme\", \"password\":\"1234\"}"))
                .andExpect(status().isOk());
    }

    @Test
    public void createUserWithNoPassword() throws Exception {
        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\":\"guilherme\", \"password\":\"\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createUserWithNoUsername() throws Exception {
        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\":\"\", \"password\":\"1234\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DirtiesContext( methodMode = AFTER_METHOD )
    public void createUserWithSameUsername() throws Exception {
        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\":\"guilherme\", \"password\":\"1234\"}"))
                .andExpect(status().isOk());

        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\":\"guilherme\", \"password\":\"5678\"}"))
                .andExpect(status().isConflict());
    }
}