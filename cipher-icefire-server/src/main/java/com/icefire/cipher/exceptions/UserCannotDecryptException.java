package com.icefire.cipher.exceptions;

public class UserCannotDecryptException extends Exception{
    public UserCannotDecryptException(String message) {
        super(message);
    }
}
