package com.icefire.cipher.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Column;

import static javax.persistence.GenerationType.AUTO;

@Entity(name = "Users")
public class User {

    @Id
    @GeneratedValue(strategy= AUTO)
    private Long id;
    @Column(unique = true)
    private String username;
    private String password;

    @OneToOne
    private UserKeyPair userKeyPair;

    protected User() {}

    public User(String username, String password, UserKeyPair userKeyPair) {
        this.username = username;
        this.password = password;
        this.userKeyPair = userKeyPair;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public UserKeyPair getUserKeyPair() {
        return userKeyPair;
    }
}
