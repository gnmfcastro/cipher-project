package com.icefire.cipher.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;

import static javax.persistence.GenerationType.AUTO;

@Entity(name = "UserKeyPairs")
public class UserKeyPair {

    @Id
    @GeneratedValue(strategy= AUTO)
    private Long id;
    @Lob
    private byte[] privateKey;
    @Lob
    private byte[] publicKey;

    public UserKeyPair() {}

    public UserKeyPair(byte[] privateKey, byte[] publicKey) {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
    }

    public byte[] getPrivateKey() {
        return privateKey;
    }

    public byte[] getPublicKey() {
        return publicKey;
    }
}
