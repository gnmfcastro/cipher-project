package com.icefire.cipher.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import static javax.persistence.GenerationType.AUTO;

@Entity(name = "CipherValue")
public class CipherValue {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    @Lob
    private String encryptedValue;
    @ManyToOne
    private User user;

    public CipherValue() {}

    public CipherValue(User user, String encryptedValue) {
        this.user = user;
        this.encryptedValue = encryptedValue;
    }

    public User getUser() {
        return user;
    }

    public String getEncryptedValue() {
        return encryptedValue;
    }
}
