package com.icefire.cipher.repositories;

import com.icefire.cipher.models.CipherValue;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CipherValuesRepository extends CrudRepository<CipherValue, Long> {
    List<CipherValue> findByUserId(Long userId);
}
