package com.icefire.cipher.repositories;

import com.icefire.cipher.models.UserKeyPair;
import org.springframework.data.repository.CrudRepository;

public interface UserKeyPairRepository extends CrudRepository<UserKeyPair, Long> {
}
