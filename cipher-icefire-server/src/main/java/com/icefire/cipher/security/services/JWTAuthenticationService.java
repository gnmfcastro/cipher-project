package com.icefire.cipher.security.services;

import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

import static io.jsonwebtoken.SignatureAlgorithm.HS512;
import static java.lang.String.format;
import static java.lang.System.currentTimeMillis;
import static java.util.Collections.emptyList;
import static org.springframework.http.HttpHeaders.*;

public class JWTAuthenticationService {

    private static final long EXPIRATION_TIME = 860_000_000;
    private static final String SECRET = "IceFireRules";
    private static final String TOKEN_PREFIX = "Bearer";
    private static final String X_AUTHORIZATION = "X-Authorization";

    public static void addAuthentication(HttpServletResponse response, String username) {
        String JWT = createToken(username);
        response.addHeader(X_AUTHORIZATION, format("%s %s", TOKEN_PREFIX, JWT));
        response.addHeader(SET_COOKIE, format("%s=%s; HttpOnly", X_AUTHORIZATION, JWT));
    }

    public static Authentication getAuthentication(HttpServletRequest request) {
        String token = getTokenFromRequest(request);

        if (token == null || token.isEmpty()) {
            return null;
        }

        String user = Jwts.parser()
                .setSigningKey(SECRET)
                .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                .getBody()
                .getSubject();

        return user != null ? new UsernamePasswordAuthenticationToken(user, null, emptyList()) : null;
    }

    public static String createToken(String username) {
        return Jwts.builder()
                .setSubject(username)
                .setExpiration(new Date(currentTimeMillis() + EXPIRATION_TIME))
                .signWith(HS512, SECRET)
                .compact();
    }

    private static String getTokenFromRequest(HttpServletRequest request) {
        String token = request.getHeader(X_AUTHORIZATION);

        if( token == null ) {
            Cookie[] cookies = request.getCookies();
            if(cookies != null ) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals(X_AUTHORIZATION)) {
                        token = cookie.getValue();
                        break;
                    }
                }
            }
        }

        return token;
    }

}
