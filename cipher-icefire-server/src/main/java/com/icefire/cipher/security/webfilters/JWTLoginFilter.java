package com.icefire.cipher.security.webfilters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.icefire.cipher.models.User;
import com.icefire.cipher.security.dto.UserCredentialsRequest;
import com.icefire.cipher.security.services.JWTAuthenticationService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

import static com.icefire.cipher.security.services.JWTAuthenticationService.*;
import static java.util.Collections.*;

public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {

    public JWTLoginFilter(String url, AuthenticationManager authenticationManager) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authenticationManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException {
        UserCredentialsRequest userCredentialsRequest = new ObjectMapper()
                .readValue(request.getInputStream(), UserCredentialsRequest.class);

        return getAuthenticationManager().authenticate(
                new UsernamePasswordAuthenticationToken(
                        userCredentialsRequest.getUsername(),
                        userCredentialsRequest.getPassword(),
                        emptyList()
                )
        );
    }

    @Override
    protected void successfulAuthentication(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain,
            Authentication auth) {
        addAuthentication(response, auth.getName());
    }


}
