package com.icefire.cipher.controllers;

import com.icefire.cipher.controllers.dto.UserCreationRequest;
import com.icefire.cipher.exceptions.UserAlreadyExistsException;
import com.icefire.cipher.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("users")
public class UsersController {

    private UserService userService;

    @Autowired
    public UsersController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity createUser(@Valid @RequestBody UserCreationRequest userCreationRequest) throws UserAlreadyExistsException {
        userService.createUser(userCreationRequest.getUsername(), userCreationRequest.getPassword());
        return ResponseEntity.ok().build();
    }

}
