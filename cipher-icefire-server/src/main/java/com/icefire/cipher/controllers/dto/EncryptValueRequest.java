package com.icefire.cipher.controllers.dto;

import javax.validation.constraints.NotNull;

public class EncryptValueRequest {

    @NotNull
    private String value;

    public EncryptValueRequest() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
