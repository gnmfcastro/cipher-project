package com.icefire.cipher.controllers;

import com.icefire.cipher.controllers.dto.EncryptValueRequest;
import com.icefire.cipher.exceptions.UserCannotDecryptException;
import com.icefire.cipher.services.CipherService;
import com.icefire.cipher.services.dto.EncryptedValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.security.core.context.SecurityContextHolder.getContext;

@RestController
@RequestMapping("ciphers")
public class CipherController {

    private CipherService cipherService;

    @Autowired
    public CipherController(CipherService cipherService) {
        this.cipherService = cipherService;
    }

    @GetMapping
    public ResponseEntity listMyEncryptedValues() {
        String requesterUsername = getContext().getAuthentication().getName();
        return ResponseEntity.ok(cipherService.listEncryptDateForUser(requesterUsername));
    }

    @PostMapping("encrypt")
    public ResponseEntity encrypt(@Valid @RequestBody EncryptValueRequest encryptValueRequest) {
        String requesterUsername = getContext().getAuthentication().getName();
        String valueToEncrypt = encryptValueRequest.getValue();
        EncryptedValue encryptedValue = cipherService.encryptDataForUser(requesterUsername, valueToEncrypt);
        return ResponseEntity.ok(encryptedValue);
    }

    @GetMapping("decrypt/{value}")
    public ResponseEntity decrypt(@PathVariable("value") String encryptedValue) throws UserCannotDecryptException {
        String requesterUsername = getContext().getAuthentication().getName();
        String decryptedValue = cipherService.decryptDataForUser(requesterUsername, encryptedValue);
        return ResponseEntity.ok(decryptedValue);
    }

}
