package com.icefire.cipher.controllers.dto;

import javax.validation.constraints.NotNull;

public class UserCreationRequest {

    @NotNull
    private String username;

    @NotNull
    private String password;
    
    public UserCreationRequest() {}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
