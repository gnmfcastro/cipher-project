package com.icefire.cipher.controllers.advices;

import com.icefire.cipher.controllers.UsersController;
import com.icefire.cipher.controllers.dto.ErrorResponse;
import com.icefire.cipher.exceptions.UserAlreadyExistsException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;

@ControllerAdvice(basePackageClasses = UsersController.class)
public class UsersControllerAdvice {

    @ExceptionHandler(UserAlreadyExistsException.class)
    public final ResponseEntity<ErrorResponse> handleUserDuplicated(UserAlreadyExistsException e) {
        return new ResponseEntity<>(new ErrorResponse(e.getMessage()), CONFLICT);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public final ResponseEntity<ErrorResponse> handleInvalidUsernameOrPassword(IllegalArgumentException e) {
        return new ResponseEntity<>(new ErrorResponse(e.getMessage()), BAD_REQUEST);
    }
}
