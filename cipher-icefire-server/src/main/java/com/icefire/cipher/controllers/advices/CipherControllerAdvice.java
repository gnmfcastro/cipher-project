package com.icefire.cipher.controllers.advices;

import com.icefire.cipher.controllers.CipherController;
import com.icefire.cipher.controllers.dto.ErrorResponse;
import com.icefire.cipher.exceptions.UserCannotDecryptException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

@ControllerAdvice(basePackageClasses = CipherController.class)
public class CipherControllerAdvice {
    @ExceptionHandler(UserCannotDecryptException.class)
    public final ResponseEntity<ErrorResponse> handleUserCannotDecryptMessage(UserCannotDecryptException e) {
        return new ResponseEntity<>(new ErrorResponse(e.getMessage()), UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public final ResponseEntity<ErrorResponse> handleInvalidValue(IllegalArgumentException e) {
        return new ResponseEntity<>(new ErrorResponse(e.getMessage()), BAD_REQUEST);
    }
}
