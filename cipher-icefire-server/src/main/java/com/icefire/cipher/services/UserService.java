package com.icefire.cipher.services;

import com.icefire.cipher.exceptions.UserAlreadyExistsException;
import com.icefire.cipher.models.User;
import com.icefire.cipher.models.UserKeyPair;
import com.icefire.cipher.repositories.UserRepository;
import com.icefire.cipher.security.dto.UserPrincipalDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Service
public class UserService implements UserDetailsService {

    private UserKeyPairService userKeyPairService;
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, UserKeyPairService userKeyPairService) {
        this.userRepository = userRepository;
        this.userKeyPairService = userKeyPairService;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    public void createUser(String username, String password) throws UserAlreadyExistsException {
        validateUsernameAndPassword(username, password);

        if(usernameExists(username)) {
            throw new UserAlreadyExistsException(format("%s already exists", username));
        }

        UserKeyPair userKeyPair = userKeyPairService.generateAndCreateKeyPair();
        User user = new User(username, passwordEncoder.encode(password), userKeyPair);
        userRepository.save(user);
    }

    @Transactional(readOnly = true)
    public User getUserByUsername(String username) {
        if(username == null ) throw new IllegalArgumentException("Empty username");

        User user = userRepository.findByUsername(username);

        if(user == null) {
            throw new UsernameNotFoundException(username);
        }

        return user;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) {
        return new UserPrincipalDetails(getUserByUsername(username));
    }

    private void validateUsernameAndPassword(String username, String password) {
        if(isEmpty(username) || isEmpty(password)) {
            throw new IllegalArgumentException("Invalid username or password");
        }
    }

    private boolean usernameExists(String username) {
        User user = userRepository.findByUsername(username);
        return user != null;
    }
}
