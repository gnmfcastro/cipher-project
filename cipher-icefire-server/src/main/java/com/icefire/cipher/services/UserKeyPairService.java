package com.icefire.cipher.services;

import com.icefire.cipher.models.UserKeyPair;
import com.icefire.cipher.repositories.UserKeyPairRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

@Service
public class UserKeyPairService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserKeyPairService.class);

    private static final String KEY_PAIR_ALGORITHM = "RSA";

    private UserKeyPairRepository userKeyPairRepository;

    @Autowired
    public UserKeyPairService(UserKeyPairRepository userKeyPairRepository) {
        this.userKeyPairRepository = userKeyPairRepository;
    }

    @Transactional
    UserKeyPair generateAndCreateKeyPair() {
        KeyPair keyPair = generateKeyPair();
        byte[] privateKey = keyPair.getPrivate().getEncoded();
        byte[] publicKey = keyPair.getPublic().getEncoded();

        UserKeyPair userKeyPair = new UserKeyPair(privateKey, publicKey);
        return userKeyPairRepository.save(userKeyPair);
    }

    private KeyPair generateKeyPair() {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(KEY_PAIR_ALGORITHM);
            return keyPairGenerator.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Invalid key pair algorithm {}", KEY_PAIR_ALGORITHM);
            throw new RuntimeException(e);
        }
    }
}
