package com.icefire.cipher.services;

import com.icefire.cipher.exceptions.UserCannotDecryptException;
import com.icefire.cipher.models.CipherValue;
import com.icefire.cipher.models.User;
import com.icefire.cipher.models.UserKeyPair;
import com.icefire.cipher.repositories.CipherValuesRepository;
import com.icefire.cipher.services.dto.EncryptedValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.List;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.tomcat.util.codec.binary.Base64.*;

@Service
public class CipherService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CipherService.class);

    private static final String USER_CANNOT_DECRYPT_MESSAGE = "User cannot decrypt the message";
    private static final String KEY_PAIR_ALGORITHM = "RSA";

    private UserService userService;
    private CipherValuesRepository cipherValuesRepository;

    @Autowired
    public CipherService(UserService userService, CipherValuesRepository cipherValuesRepository) {
        this.userService = userService;
        this.cipherValuesRepository = cipherValuesRepository;
    }

    @Transactional
    public EncryptedValue encryptDataForUser(String username, String value) {
        if(isEmpty(value)) {
           throw new IllegalArgumentException("Value cannot be empty");
        }

        User user = userService.getUserByUsername(username);
        UserKeyPair userKeyPair = user.getUserKeyPair();
        PrivateKey privateKey = getPrivateKey(userKeyPair);

        String encryptedValue = getEncryptedValue(value, privateKey);

        CipherValue cipherValue = new CipherValue(user, encryptedValue);
        cipherValuesRepository.save(cipherValue);

        String encryptedValueURLSafe = encodeBase64URLSafeString(decodeBase64(encryptedValue));
        return new EncryptedValue(encryptedValue, encryptedValueURLSafe);
    }

    @Transactional(readOnly = true)
    public List<EncryptedValue> listEncryptDateForUser(String username) {
        User user = userService.getUserByUsername(username);

        List<CipherValue> cipherValues = cipherValuesRepository.findByUserId(user.getId());

        return cipherValues.stream().map(cipherValue -> {
            String encryptedValue = cipherValue.getEncryptedValue();
            String encryptedValueURLSafe = encodeBase64URLSafeString(decodeBase64(encryptedValue));
            return new EncryptedValue(encryptedValue, encryptedValueURLSafe);
        }).collect(toList());
    }

    @Transactional(readOnly = true)
    public String decryptDataForUser(String username, String encryptedValue) throws UserCannotDecryptException {
        User user = userService.getUserByUsername(username);
        UserKeyPair userKeyPair = user.getUserKeyPair();
        PublicKey publicKey = getPublicKey(userKeyPair);

        return getDecryptedValue(encryptedValue, publicKey);
    }

    private String getEncryptedValue(String value, PrivateKey privateKey) {
        try {
            Cipher cipher = Cipher.getInstance(KEY_PAIR_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, privateKey);
            byte[] encryptedBytes = cipher.doFinal(value.getBytes(UTF_8));
            return encodeBase64String(encryptedBytes);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | InvalidKeyException e) {
            LOGGER.error("An error has occured encrypting the value {}", value);
            throw new RuntimeException(e);
        } catch (IllegalBlockSizeException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    private String getDecryptedValue(String value, PublicKey publicKey) throws UserCannotDecryptException {
        try {
            Cipher cipher = Cipher.getInstance(KEY_PAIR_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, publicKey);
            byte[] decryptedBytes = cipher.doFinal(decodeBase64(value));
            return new String(decryptedBytes, UTF_8);
        } catch(BadPaddingException e) {
            LOGGER.info("User cannot decrypt the message {}", value);
            throw new UserCannotDecryptException(format("%s %s", USER_CANNOT_DECRYPT_MESSAGE, value));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException e) {
            LOGGER.error("An error has occured encrypting the value {}", value);
            throw new RuntimeException(e);
        }
    }

    private PublicKey getPublicKey(UserKeyPair userKeyPair) {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_PAIR_ALGORITHM);
            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(userKeyPair.getPublicKey());
            return keyFactory.generatePublic(x509EncodedKeySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            LOGGER.error("Invalid public key");
            throw new RuntimeException(e);
        }
    }

    private PrivateKey getPrivateKey(UserKeyPair userKeyPair) {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_PAIR_ALGORITHM);
            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(userKeyPair.getPrivateKey());
            return keyFactory.generatePrivate(pkcs8EncodedKeySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            LOGGER.error("Invalid private key");
            throw new RuntimeException(e);
        }
    }
}
