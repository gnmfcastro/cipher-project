package com.icefire.cipher.services.dto;

public class EncryptedValue {

    private String encryptedValue;
    private String encryptedValueUrlSafe;

    public EncryptedValue() {}

    public EncryptedValue(String encryptedValue, String encryptedValueUrlSafe) {
        this.encryptedValue = encryptedValue;
        this.encryptedValueUrlSafe = encryptedValueUrlSafe;
    }

    public String getEncryptedValue() {
        return encryptedValue;
    }

    public void setEncryptedValue(String encryptedValue) {
        this.encryptedValue = encryptedValue;
    }

    public String getEncryptedValueUrlSafe() {
        return encryptedValueUrlSafe;
    }

    public void setEncryptedValueUrlSafe(String encryptedValueUrlSafe) {
        this.encryptedValueUrlSafe = encryptedValueUrlSafe;
    }
}
