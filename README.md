# Summary

Backend:

- Authentication with JWT using simple sign strategy : 1:00 hr
- User creation and validation endpoint : 2:00 hrs
- Decrypting and encrypting endpoints : 2:00 hrs
- Decrypting validation from other users: 0:30 hrs
- Listing encrypted values from user endpoint : 0:30 hrs
- Unit tests: 1:00 hr
- Small refactors: 1:00 hr

Frontend:

- Create user page: 1:30 hr
- Login page: 2:00 hrs
- Authentication and routing functionality: 2:00 hrs
- Logout: 0:30 hr
- List encrypted values: 1:00 hr
- Encrypt values: 1:00 hr
- Decrypt values: 1:00 hr
- Unit tests: - (Didn't since werent a requisite, running out of time)

# Run aplication

To run the application on backend go to the **cipher-icefire-client** and run:

```
./mvnw spring-boot:run
```

It runs on **localhost:8080**

To run the frontend go to **cipher-icefire-client** and run:

```
npm install 
npm start
```
It runs on **localhost:3000**

Access the front - end and have fun ! :)

# Run tests

To run the application on backend go to the **cipher-icefire-server** and run:

```
./mvnw test
```

