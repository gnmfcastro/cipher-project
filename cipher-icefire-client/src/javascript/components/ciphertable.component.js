import React, { Component } from "react";

export default class CipherTable extends Component {
    constructor() {
        super();
        this.state = {}
    }

    render() {
        const { list, onRowClick } = this.props;

        return (
            <table className="ui celled table">
                <thead>
                    <tr>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        list.map((cipher, index) => {
                            return (
                                <tr key={index} onClick={() => onRowClick(cipher)}>
                                    <td data-url-safe={cipher.encryptedValueUrlSafe}
                                        data-label="value">
                                        {cipher.encryptedValue}
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        );
    }
}