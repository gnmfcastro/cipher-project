import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";

import { logout } from "../apis/auth"

export default class Menu extends Component {
    constructor() {
        super();
        this.state = {
            redirectToLogin: false
        }
    }

    onLogoutSuccess() {
        this.setState({ redirectToLogin: true })
    }

    onLogoutError() {}

    onLogout() {
        logout(this.onLogoutSuccess.bind(this), this.onLogoutError.bind(this));
    }

    render() {
        const { redirectToLogin } = this.state;

        return (
            <div>
                { redirectToLogin && <Redirect to="/login" /> }
                <div className="ui secondary pointing menu">
                    <Link to="/" className="item active">
                        Home
                    </Link>
                    <div className="right menu">
                        <Link to="" className="ui item" onClick={this.onLogout.bind(this)}>
                            Logout
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}