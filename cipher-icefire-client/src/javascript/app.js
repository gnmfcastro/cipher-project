import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";

import LoginPage from "./pages/login.page"
import SignupPage from "./pages/signup.page"
import HomePage from "./pages/home.page"

export default class App extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/login" component={LoginPage} />
        <Route exact path="/signup" component={SignupPage} />
        <Route exact path="/home" component={HomePage} />
        <Route exact path="/" component={HomePage} />
      </Switch>
    );
  }
}
