import React, { Component } from "react";

import { signup } from "../apis/signup";

import { NavLink } from "react-router-dom";

export default class SignupPage extends Component {
    constructor() {
        super();
        this.state = {
            username: "",
            password: "",
            onSuccessFeedback: "",
            onErrorFeedback: ""
        }
    }

    onSuccessSignup() {
        const { username } = this.state;
        this.setState({ 
            onSuccessFeedback: `Account ${username} created`,
            username: "",
            password: "",
            onErrorFeedback: ""
        });
    }

    onErrorSignup(error) {
        const { username } = this.state;

        if(error === "ACCOUNT_ALREADY_EXISTS") {
            this.setState({ 
                onSuccessFeedback: "",
                onErrorFeedback: `Account ${username} already exists`
            });
        }else if(error === "INVALID_VALUES") {
            this.setState({ 
                onSuccessFeedback: "",
                onErrorFeedback: `Invalid values`
            });
        } else {
            this.setState({ 
                onSuccessFeedback: "",
                onErrorFeedback: `An error has occuried`
            });
        }
    }

    submitSignup(event) {
        event.preventDefault();

        const { username, password } = this.state;
        signup(username, password, this.onSuccessSignup.bind(this), this.onErrorSignup.bind(this));
    }

    setUsername(event) {
        this.setState({username: event.target.value});
    }

    setPassword(event) {
        this.setState({password: event.target.value});
    }

    renderSuccessFeedbackMessage(message) {
        return (
            <div className="ui positive message">
                <div className="header">
                    {message}
                </div>
            </div>
        );
    }

    renderErrorFeedbackMessage(message) {
        return (
            <div className="ui warning message">
                <div className="header">
                    {message}
                </div>
            </div>
        );
    }

    renderHintMessage() {
        return (
            <div className="ui message"> 
                    Already a member? <NavLink to="/login">Login</NavLink>
            </div>
        );
    }

    renderSignupForm() {
        const { username, password } = this.state;
        
        return (
            <form className="ui large form" onSubmit={this.submitSignup.bind(this)}>
                <div className="ui stacked segment">
                    <div className="field">
                        <label>Username</label>
                        <input type="text" 
                                name="username" 
                                placeholder="Username" 
                                value={username}
                            onChange={this.setUsername.bind(this)}/>
                    </div>
                    <div className="field">
                        <label>Password</label>
                        <input type="password" 
                                name="password" 
                                placeholder="Password" 
                                value={password}
                                onChange={this.setPassword.bind(this)}/>
                    </div>
                    <button className="ui fluid large primary submit button" type="submit">Sign Up</button>
                </div>
            </form>
        )
    }

    render() {
        const { onSuccessFeedback, onErrorFeedback } = this.state;
        return (
            <div className="ui middle aligned center aligned grid">
                <div className="column">
                    <h1>Sign Up</h1>
                    { onSuccessFeedback && this.renderSuccessFeedbackMessage(onSuccessFeedback) }
                    { onErrorFeedback && this.renderErrorFeedbackMessage(onErrorFeedback) }
                    { this.renderSignupForm() } 
                    { this.renderHintMessage() }
                </div>
            </div>
        );
    }
}