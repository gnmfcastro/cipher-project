import React, { Component } from "react";
import { NavLink, Redirect } from "react-router-dom";

import "../../css/login.css";

import { login } from "../apis/auth"

export default class LoginPage extends Component {
    constructor() {
        super();
        this.state = {
            username: "",
            password: "",
            redirectToHome: false,
            loginErrorFeedback: ""
        }
    }

    onLoginSuccess() {
        this.setState({ redirectToHome: true });
    }

    onLoginError(error) {
        const loginErrorFeedback = error === "UNAUTHORIZED" ? "Invalid Username or Password" : "An error has occured"
        this.setState({ loginErrorFeedback });
    }

    submitLogin(event) {
        event.preventDefault();

        const { username, password } = this.state;
        login(username, password, this.onLoginSuccess.bind(this), this.onLoginError.bind(this));
    }

    setUsername(event) {
        this.setState({ username: event.target.value });
    }

    setPassword(event) {
        this.setState({ password: event.target.value });
    }

    redirectToHome() {
        return (
            <Redirect to="/home" />
        );
    }

    renderLoginForm() {
        const { username, password, loginErrorFeedback} = this.state;

        return (
            <div>
                <h1>Login</h1>
                { loginErrorFeedback && this.renderFeedbackMessage() }
                <form className="ui large form" onSubmit={this.submitLogin.bind(this)}>
                    <div className="ui stacked segment">
                        <div className="field">
                            <label>Username</label>
                            <input type="text" 
                                name="username" 
                                placeholder="Username" 
                                value={username}
                                onChange={this.setUsername.bind(this)}/>
                        </div>
                        <div className="field">
                            <label>Password</label>
                            <input type="password" 
                                name="password" 
                                placeholder="Password" 
                                value={password}
                                onChange={this.setPassword.bind(this)}/>
                        </div>
                        <button className="ui fluid large primary submit button" type="submit">Login</button>
                    </div>
                </form>
            </div>
        );
    }

    renderFeedbackMessage() {
        const { loginErrorFeedback } = this.state;

        return (
            <div className="ui warning message">
                <div className="header">
                    {loginErrorFeedback}
                </div>
            </div>
        );
    }

    renderSignUpMessage() {
        return (
            <div className="ui message"> 
                New to us? <NavLink to="/signup">Sign Up</NavLink>
            </div>
        );
    }

    render() {
        const { redirectToHome } = this.state;

        return (
        <div className="ui middle aligned center aligned grid">
            {redirectToHome && this.redirectToHome() }
            <div className="column">
                { this.renderLoginForm() }
                { this.renderSignUpMessage() }
            </div>
        </div>
        );
    }
}