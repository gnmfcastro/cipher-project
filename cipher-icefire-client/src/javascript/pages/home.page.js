import React, { Component } from "react";
import { Redirect } from "react-router-dom";

import Menu from "../components/menu.component";
import CipherTable from "../components/ciphertable.component";

import "../../css/home.css";

import { listEncryptedValues, encryptValue, decryptValue } from "../apis/cipher"

export default class HomePage extends Component {
    constructor() {
        super();
        this.state = {
            cipherValue: "",
            cipherValueUrlSafe: "",
            cipherList: [],
            needLogin: false,
            canDecrypt: false,
            onErrorFeedback: ""
        }
    }

    componentWillMount() {
        listEncryptedValues(this.onListLoaded.bind(this), this.onListError.bind(this));
    }
    
    onListLoaded(data) {
        this.setState({ 
            cipherList : data,
            onErrorFeedback: ""
        });
    }

    onListError(error) {
        if(error === "UNAUTHORIZED") {
            this.setState({ needLogin: true })
        } else {
            this.setState({ onErrorFeedback: "An error has occuried" })
        }
    }

    onClickRow(cipher) {
        this.setState({ 
            cipherValue : cipher.encryptedValue,
            cipherValueUrlSafe : cipher.encryptedValueUrlSafe,
            canDecrypt: true 
        });
    }

    setCipherValue(event) {
        const value = event.target.value || "";
        const canDecrypt = value ? this.state.canDecrypt : false;
        this.setState({ cipherValue: value, canDecrypt })
    }

    onEncryptSuccess(data) {
        const { cipherList } = this.state;
        const newCipherList = [ ...cipherList, data ];
        this.setState({ 
            cipherList: newCipherList,
            cipherValue: "",
            onErrorFeedback: "" 
        });
    }

    onEncryptDecryptError(error) {
        if(error === "UNAUTHORIZED") {
            this.setState({ needLogin: true })
        } else if (error === "INVALID_VALUE") {
            this.setState({ onErrorFeedback: "This value can't be decrypted" })
        } else {
            this.setState({ onErrorFeedback: "An error has occuried" })
        }
    }

    onDecryptSuccess(data) {
        this.setState({ 
            cipherValue : data, 
            canDecrypt: false,
            onErrorFeedback: ""
        });
    }

    onEncrypt() {
        const { cipherValue } = this.state;
        cipherValue && encryptValue( cipherValue, 
            this.onEncryptSuccess.bind(this), 
            this.onEncryptDecryptError.bind(this)
        );
    }

    onDecrypt() {
        const { cipherValueUrlSafe } = this.state;
        cipherValueUrlSafe && decryptValue( cipherValueUrlSafe, 
            this.onDecryptSuccess.bind(this), 
            this.onEncryptDecryptError.bind(this)
        );
    }

    redirectToLogin() {
        return ( <Redirect to="/login" /> );
    }

    renderErrorFeedback(message) {
        return (
            <div className="ui warning message">
                <div className="header">
                    {message}
                </div>
            </div>
        );
    }

    renderCipherForm() {
        const { cipherValue, canDecrypt } = this.state;

        return (
            <div className="ui fluid action input">
                <input type="text"
                    name="cipherValue"
                    placeholder="Text"
                    value={cipherValue}
                    onChange={this.setCipherValue.bind(this)} />
                <div className="ui primary button" onClick={this.onEncrypt.bind(this)}>
                    encrypt
                </div>
                { canDecrypt ?
                    <button className="ui button" onClick={this.onDecrypt.bind(this)}>decrypt</button> : 
                    <button className="ui button" disabled>decrypt</button>
                }
            </div>
        );
    }

    render() {
        const { cipherList, needLogin, onErrorFeedback } = this.state;

        return (
            <div>
                { needLogin && this.redirectToLogin() }
                <Menu/>
                <div className = "ui container">
                    <h1>Cipher Table</h1>
                    { onErrorFeedback && this.renderErrorFeedback(onErrorFeedback) }
                    { this.renderCipherForm() }
                    <CipherTable list={cipherList} onRowClick={this.onClickRow.bind(this)}/>
                </div>
            </div>
        );
    }
}