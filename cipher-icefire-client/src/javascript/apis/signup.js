const API_HOST = "http://localhost:8080";
const SIGNUP_ENDPOINT = `${API_HOST}/users`;

const ACCOUNT_ALREADY_EXISTS = "ACCOUNT_ALREADY_EXISTS";
const INVALID_VALUES = "INVALID_VALUES";
const UNKNOWN_ERROR = "UNKNOWN_ERROR";

const headers = new Headers();
headers.append("Content-Type", "application/json");

const signup = async (username, password, onSuccess = () => {} , onFail = () => {}) => {
    const options = {
        method: "POST",
        headers,
        body: JSON.stringify({ username, password})
    }
    
    const response = await fetch(SIGNUP_ENDPOINT, options);
    if (response.ok) {
        onSuccess();
        return;
    }

    if (response.status === 409) {
        onFail(ACCOUNT_ALREADY_EXISTS);
        return;
    }

    if (response.status === 400) {
        onFail(INVALID_VALUES);
        return;
    }

    onFail(UNKNOWN_ERROR);
} 

export { signup }