const API_HOST = "http://localhost:8080";
const LOGIN_ENDPOINT = `${API_HOST}/login`;
const LOGOUT_ENDPOINT = `${API_HOST}/auth/logout`;

const UNAUTHORIZED = "UNAUTHORIZED";
const UNKNOWN_ERROR = "UNKNOWN_ERROR";

const headers = new Headers();
headers.append("Content-Type", "application/json");

const login = async (username, password, onSuccess = () => {} , onError = () => {}) => {
    const options = {
        method: "POST",
        headers,
        credentials: "include",
        body: JSON.stringify({ username, password})
    }

    const response = await fetch(LOGIN_ENDPOINT, options);
    if (response.ok) {
        onSuccess();
        return;
    }

    if (response.status === 401 || response.status === 403 ) {
        onError(UNAUTHORIZED);
        return;
    }

    onError(UNKNOWN_ERROR);
} 

const logout = async (onSuccess = () => {} , onError = () => {}) => {
    const options = {
        method: "DELETE",
        headers,
        credentials: "include"
    }

    try {
        await fetch(LOGOUT_ENDPOINT, options);
        onSuccess();
    } catch(e) {
        onError();
    }
} 

export { login, logout }