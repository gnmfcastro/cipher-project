const API_HOST = "http://localhost:8080";
const LIST_CIPHERS_ENDPOINT = `${API_HOST}/ciphers`;
const ENCRYPT_ENDPOINT = `${API_HOST}/ciphers/encrypt`;
const DECRYPT_ENDPOINT = `${API_HOST}/ciphers/decrypt`;

const headers = new Headers();
headers.append("Content-Type", "application/json");

const UNAUTHORIZED = "UNAUTHORIZED";
const UNKNOWN_ERROR = "UNKNOWN_ERROR";
const INVALID_VALUE = "INVALID_VALUE";

const listEncryptedValues = async ( onSuccess = () => {} , onError = () => {}) => {
    const options = {
        method: "GET",
        headers,
        credentials: "include"
    }

    const response = await fetch(LIST_CIPHERS_ENDPOINT, options);
    if (response.ok) {
        const data = await response.json();
        onSuccess(data);
        return;
    }

    if (response.status === 401 || response.status === 403) {
        onError(UNAUTHORIZED);
        return;
    }

    onError(UNKNOWN_ERROR);
} 

const encryptValue = async ( value, onSuccess = () => {} , onError = () => {}) => {
    const options = {
        method: "POST",
        headers,
        credentials: "include",
        body: JSON.stringify({ value })
    }

    const response = await fetch(ENCRYPT_ENDPOINT, options);
    if (response.ok) {
        const data = await response.json();
        onSuccess(data);
        return;
    }
    
    if (response.status === 401 || response.status === 403) {
        onError(UNAUTHORIZED);
        return;
    }

    if (response.status === 400) {
        onError(INVALID_VALUE);
        return;
    }

    onError(UNKNOWN_ERROR);
} 

const decryptValue = async ( value, onSuccess = () => {} , onError = () => {}) => {
    const options = {
        method: "GET",
        headers,
        credentials: "include"
    }

    const response = await fetch(`${DECRYPT_ENDPOINT}/${value}`, options);
    if (response.ok) {
        const data = await response.text();
        onSuccess(data);
        return;
    }
    
    if (response.status === 401 || response.status === 403) {
        onError(UNAUTHORIZED);
        return;
    }

    if (response.status === 400) {
        onError(INVALID_VALUE);
        return;
    }

    onError(UNKNOWN_ERROR);
}

export { listEncryptedValues, encryptValue, decryptValue }