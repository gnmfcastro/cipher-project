import React from "react";
import ReactDOM from "react-dom";

import * as serviceWorker from "./javascript/serviceWorker";

import { BrowserRouter } from "react-router-dom";
import App from "./javascript/app";

import 'semantic-ui-css/semantic.min.css'

ReactDOM.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>,
    document.getElementById("root")
);
serviceWorker.unregister();
